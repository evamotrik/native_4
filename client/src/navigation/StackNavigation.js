import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { Registration } from '../screens/Registration';
import { Login } from '../screens/Login';
import { Order } from '../screens/Order';
import DrawerNavigator from './DrawerNavigator'

const Stack = createNativeStackNavigator();

export default function StackNavigator() {

    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login"
                screenOptions={{
                    headerStyle: {
                        backgroundColor: "#9AC4F8",
                    },
                    headerTintColor: "white",
                    headerBackTitle: "Back",
                }}>
                <Stack.Screen name="Registration" component={Registration} initialParams={{ isRegistered: false }} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Order" component={Order} />
                <Stack.Screen name="Main" component={DrawerNavigator} options={{headerShown:false}} />
            </Stack.Navigator>
            
        </NavigationContainer>
    )
}

 
  