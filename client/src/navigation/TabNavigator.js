import React, {useContext,useState, useEffect} from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Main } from '../screens/Main';
import { Products } from '../screens/Products';
import { Cart } from '../screens/Cart';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Context from "../utils/context";


const Tab = createBottomTabNavigator();

export default BottomTabNavigator = () => {

  const context = useContext(Context);

  const [length, setLength] = useState("");

  useEffect(() => {
      setLength(context.userState.productsLength);
  }, [context.userState.productsLength])

  return (

    <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'Profile') {
          iconName = focused
            ? 'person'
            : 'person-outline';
        } else if (route.name === 'Products') {
          iconName = focused ? 'ios-list' : 'ios-list-outline';
        }
        else if(route.name === 'Cart'){
          iconName = focused ? 'cart' : 'cart-outline';
        }

        return <Ionicons name={iconName} size={size} color={color} />;
      },
      tabBarActiveTintColor: 'tomato',
      tabBarInactiveTintColor: 'gray',
    })}
    >
      <Tab.Screen name="Profile" component={Main}
        options={{
          headerShown: false, tabBarLabel: 'Profile',
        }}
      />
      <Tab.Screen name="Products" component={Products}
        options={{
          headerShown: false, 
        }} />
      <Tab.Screen name="Cart" initialParams={{param:true}} component={Cart} options={{ tabBarBadge: length }} />
    </Tab.Navigator>

  );
};
