import React, { useState, useEffect, useContext } from 'react';
import { Modal, ScrollView, Button, StyleSheet, Text, View, Alert } from 'react-native';
import { getCartById } from '../services/http.service';
import { createOrder } from '../services/http.service';
import * as Notifications from 'expo-notifications';
import Context from '../utils/context';

Notifications.setNotificationHandler({
    handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: false,
        shouldSetBadge: false,
    }),
});

export const Cart = ({ navigation, route }) => {

   
    const [products, setProducts] = useState([]);

    useEffect(() => {
        (async () => {
            const result = await getCartById();
            setProducts(result)
        })()
    }, [route.params])

    const [visible, setVisible] = React.useState(false);

    async function orderBtn() {
        const res = await createOrder();
        if (res) {
            navigation.navigate('Order')
        }
    }

    const hideModal = () => setVisible(false);
    const showModal = () => setVisible(true);

    async function orderNotification() {
        if (products.cart.products.length != 0) {
            await Notifications.scheduleNotificationAsync({
                content: {
                    title: "You've got product's in your cart! 🛒",
                    body: "Don't you want to make an order?",
                    data: { data: 'goes here' },
                },
                trigger: { seconds: 6000 },
            });
        }
    }
    
    orderNotification()

    return (
        <View style={styles.view}>
            <ScrollView>

                {
                    products && products.cart && products.cart.products.map(product => (
                        <View style={styles.product}>
                            <Text style={styles.productName}>{product.name}</Text>
                            <Text>Price: {product.price.toString()} $</Text>
                        </View>
                    ))
                }

                <Button title="to order" onPress={showModal} />
            </ScrollView>


            <Modal
                visible={visible}
                onDismiss={hideModal}
                animationType="slide"
                transparent={true}>

                <View style={styles.modalView}>
                    <Text>Are you sure you want to make an order?</Text>
                    <View style={styles.buttons}>
                        <Button title="Yes" onPress={orderBtn} />
                        <Button title="No" onPress={hideModal} />
                    </View>

                </View>
            </Modal>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    buttons: {
        marginTop: 15,
        flexDirection: 'row'
    },
    product: {
        margin: 10
    },
    productName: {
        fontSize: 17,
        fontWeight: 'bold'
    }
});


