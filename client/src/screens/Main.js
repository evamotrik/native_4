import { StyleSheet, View, Text } from 'react-native';
import React, { useContext } from 'react';
import Context from '../utils/context'

export const Main = () => {

  const context = useContext(Context);

  return (
    <View style={styles.view}>
      <Text>Email: {context.userState.email}</Text>
      <Text>Password: {context.userState.password}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
});


