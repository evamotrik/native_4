import React, { useContext, useEffect, useState } from 'react';
import { Alert, StyleSheet, TextInput, Button, View } from 'react-native';
import Context from '../utils/context'
import { registration } from '../services/http.service';

export const Registration = ({ navigation, route }) => {

  const context = useContext(Context);
  const { isRegistered } = route.params;

  useEffect(() => {
    if (isRegistered) {
      setName(context.userState.name),
      setSurname(context.userState.surname),
      setEmail(context.userState.email),
      setPassword(context.userState.password),
      setId(context.userState.id)
    }
  }, [])

  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');


  async function registrationBtn() {
    if (email === '' || password === '' || name === '' || surname === '') {
      return Alert.alert(`Fields shouldn't be empty!`
      )
    }

    const res = await registration({ name, surname, email, password });
    if (res) {
      navigation.navigate('Login')
    }

  }

  return (
    <View style={styles.view} >
      <TextInput placeholder="Name"
        value={name}
        onChangeText={text => setName(text)}
        style={styles.input} />
      <TextInput placeholder="Surname"
        value={surname}
        onChangeText={text => setSurname(text)}
        style={styles.input} />
      <TextInput placeholder="Email"
        value={email}
        onChangeText={text => setEmail(text)}
        style={styles.input} />
      <TextInput placeholder="Password"
        value={password}
        onChangeText={text => setPassword(text)}
        style={styles.input} />
      <Button title={(route.params.isRegistered === true ? 'Sing up' : 'Save')}
        onPress={registrationBtn} />
    </View>


  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  input: {
    height: 40,
    marginBottom: 5,
    width: 200,
    borderColor: 'gray',
    borderWidth: 1,
    padding: 5
  },

  container: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: 'row'
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  containerSwitch: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

