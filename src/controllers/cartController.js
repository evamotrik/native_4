import Cart from "../models/Cart.js"
import User from "../models/User.js"

class CartController {
    async cartCreate(req, res) {
        try {
            let cart = new Cart();
            let user = await User.findOne({ _id: req.user.id });

            cart.user = req.user.id;
            await cart.save();

            user.cart = cart._id;
            await user.save();
            res.json({ cart });
        } catch (e) {
            res.status(400).json({ message: "Ошибка создания корзины" })
        }
    }

    async addProducts(req, res) {
        try {
            let user = await User.findOne({ _id: req.user.id })
            let cart = await Cart.findOne({ _id: user.cart });
            let product = req.body.id;
            cart.products.push(product);
            await cart.save();
            res.json({ message: `Продукт добавлен в корзину` });
        } catch (e) {
            res.status(400).json({ message: "Ошибка добавления в корзину" })
        }
    }

    async getById(req, res) {
        try {
            let user = await User.findOne({ _id: req.user.id })
            const cart = await Cart.findOne({ _id: user.cart })
            res.json({ cart });

        } catch (e) {
            res.status(400).json({ message: "Get Carts ID error -> " + e })
        }
    }
}

export default new CartController;

