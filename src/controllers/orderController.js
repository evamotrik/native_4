import Order from "../models/Order.js"
import User from "../models/User.js"
import Cart from "../models/Cart.js"
import e from "express";


class OrderController {
    async orderCreate(req, res) {
        try {
            let order = new Order();
            let user = await User.findOne({ _id: req.user.id });
            let cart = await Cart.findOne({ _id: user.cart })
            order.user = req.user.id;
            order.cart = cart.products;
            await order.save();

            cart.products = []
            await cart.save() 
            
            user.order.push(order._id);
            await user.save();
            res.json({ order });
        } catch (e) {
            res.status(400).json({ message: "Ошибка созания заказа" })
        }
    }

    async deleteOrder(req, res) {
        try {
            const order = await Order.findOne({ _id: req.params.id })
            if (order) {
                if (req.user.role === 'ADMIN' || req.user.id.toString() === order.user.toString()) {
                    deleteFromUser(order._id, order.user)
                    Order.deleteOne({ _id: req.params.id }, function (err, doc) {
                        if (err) return res.send("error");
                        res.send(`Заказ удален`);
                    });
                } else {
                    res.json({ message: "Заказ не принадлежит Вам" })
                }
            }
            else res.send(`Такого заказа не существует`);
        } catch (e) {
            res.status(400).json({ message: "Deleting order error -> " + e })
        }
    }

    async getById(req, res) {
        try {
            const order = await Order.findOne({ _id: req.params.id })
            if (order) {
                res.json({ order })
            }
            else res.send(`Такого заказа не существует`);

        } catch (e) {
            res.status(400).json({ message: "Get Orders ID error -> " + e })
        }
    }

    // async getAll (req, res){
    //     try{
    //         let order
    //         if(req.user.role.toString() === 'ADMIN'){
    //             order = await Order.find()
    //             res.json({message: order})
    //         }else{
    //             order = await Order.find({user: req.user.id})
    //             res.json({message: order})
    //         }
    //     }catch (e){
    //         res.status(400).json({message: "error -> " + e})
    //     } 
    // }

    async getAll(req, res) {
        try {
            let order
            order = await Order.find({ user: req.user.id })
            res.json(order)
        } catch (e) {
            res.status(400).json({ message: "error -> " + e })
        }
    }
}

const deleteFromUser = async (orderId, userId) => {
    const user = await User.findOne({ _id: userId })
    for (let i = 0; i < user.order.length; i++) {
        if (user.order[i].toString() === orderId.toString()) {
            user.order.splice(i, 1)
            await user.save()
            break
        }
    }
}


export default new OrderController;
