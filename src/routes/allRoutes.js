import express from 'express'
import authRoutes from "./localRoutes/authRoutes.js"
import categoryRoutes from './localRoutes/categoryRoutes.js'
import productRoutes from './localRoutes/productRoutes.js'
import cartRoutes from './localRoutes/cartRoutes.js'
import orderRoutes from './localRoutes/orderRoutes.js'

const router = express.Router()

router.use('/auth', authRoutes);
router.use('/category', categoryRoutes);
router.use('/product', productRoutes);
router.use('/cart', cartRoutes);
router.use('/order', orderRoutes)

export default router


