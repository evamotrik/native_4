import express from 'express'
import productController from '../../controllers/productController.js' 
const router = express.Router()

router.post("/create", productController.productCreate);    
router.delete("/:id", productController.deleteProduct);
router.post("/update/:id", productController.updateProduct);
router.get("/getbyId/:id", productController.getById);
router.get("/getAll", productController.getAll);

export default router;