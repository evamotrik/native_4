import express from 'express'
import orderController from '../../controllers/orderController.js' 
import authMiddleware from '../../middleware/authMiddleware.js'
const router = express.Router()

router.post("/create", authMiddleware, orderController.orderCreate); 
router.delete("/:id", authMiddleware, orderController.deleteOrder);
router.get("/getbyId/:id",authMiddleware, orderController.getById);
router.get("/getAll", authMiddleware, orderController.getAll);


export default router;