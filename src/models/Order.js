import pkg from "mongoose"
const {model, Schema} = pkg

const orderSchema = new Schema({
    cart: [{
        type: Schema.Types.ObjectId,
        ref: 'Cart',
    }],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
})

export default model('Order', orderSchema)