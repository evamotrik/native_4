import pkg from "mongoose"
const {model, Schema} = pkg

const productSchema = new Schema({
    name:{
        type: String,
    },
    price:{
        type: Number,
    }
});

export default model('Product', productSchema)