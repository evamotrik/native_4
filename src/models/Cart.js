import pkg from "mongoose"
const {model, Schema} = pkg
import autopopulate from 'mongoose-autopopulate'

const cartSchema = new Schema({
    products: [{
        type: Schema.Types.ObjectId,
        ref: "Product",
        autopopulate:true
    }],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
});
cartSchema.plugin(autopopulate);
export default model('Cart', cartSchema)